#!/bin/bash

destroy_vnc() {
  docker-compose -p vnc -f build/docker-compose.yaml exec worker scripts/machine-destroy.sh;
}

destroy_infrastructure() {
  docker-compose -p vnc -f build/docker-compose.yaml down;
}

main() {
  destroy_vnc;
  destroy_infrastructure;
}

main;
