#!/bin/bash

set -euo pipefail

create_infrastructure() {
  mkdir -p /tmp/.wg-data/;
  docker-compose -p vnc -f build/docker-compose.yaml up -d --build;
}

create_vnc() {
  docker-compose -p vnc -f build/docker-compose.yaml exec worker scripts/base-image-build.sh;
  docker-compose -p vnc -f build/docker-compose.yaml exec worker scripts/image-apply.sh;
  docker-compose -p vnc -f build/docker-compose.yaml exec worker scripts/machine-apply.sh;
}

print_instructions() {
  cat <<INSTRUCTIONS
To connect to vnc server run following commands:
  sudo wg-quick up /tmp/.wg-data/vnc.conf
  vncviewer 10.0.0.1:5900
To stop vnc run:
  sudo wg-quick down /tmp/.wg-data/vnc.conf
  ./destroy.sh
INSTRUCTIONS
}

main() {
  create_infrastructure;
  create_vnc;
  print_instructions;
}

main;
