variable "token" {
  description = "Digital ocean token"
  type = string
  validation {
    condition = can(regex("^[0-9a-f]{64}$", var.token))
    error_message = "Token must be a 64-characters hex lowercase string."
  }
}

variable "mitogen" {
  description = "Mitogen plugin configuration."
  type = object({
    enable = bool
    path = string
  })
  default = {
    enable = false
    path = ""
  }
  validation {
    condition = !var.mitogen.enable || fileexists(join("/", [abspath(var.mitogen.path), "mitogen.py"]))
    error_message = "Mitogen folder must exist."
  }
}

variable "public_key" {
  description = "Public ssh key."
  type = string
}

variable "private_key" {
  description = "Private ssh key."
  type = string
}

variable "user_volume_id" {
  description = "Created volumes id."
  type = string
}

variable "image_name" {
  description = "VNC machine image name"
  type = string
}

variable "region" {
  description = "Digital ocean region to create droplet in"
  type = string
}

variable "volume_name" {
  description = "Users volume name."
  type = string
}

variable "ansible_vault_password" {
  description = "Password for ansible vault"
  type = string
}
