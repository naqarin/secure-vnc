data "digitalocean_droplet_snapshot" "vnc_image" {
  name = var.image_name
  region = var.region
  most_recent = true
}

data "digitalocean_volume" "users_volume" {
  name = var.volume_name
}

resource "digitalocean_ssh_key" "main" {
  name = "Main key"
  public_key = var.public_key
}

resource "digitalocean_droplet" "wireguard_server" {
  image = data.digitalocean_droplet_snapshot.vnc_image.id
  name = "wireguard"
  region = var.region
  size = "s-4vcpu-8gb"
  ssh_keys = [
    digitalocean_ssh_key.main.fingerprint
  ]
  tags = [
    "vpn",
    "security",
    "wireguard"
  ]
  volume_ids = [
    data.digitalocean_volume.users_volume.id
  ]

  provisioner "remote-exec" {
    connection {
      host = self.ipv4_address
      user = "root"
      type = "ssh"
      private_key = var.private_key
    }
    inline = [
      "echo 'OK!'"
    ]
  }
}

resource "local_file" "ssh_key" {
  filename = "${dirname(dirname(abspath(path.module)))}/ansible/terraform"
  sensitive_content = var.private_key
  file_permission = "0400"
}

resource "local_file" "ansible_password" {
  filename = "${dirname(dirname(abspath(path.module)))}/ansible/password"
  sensitive_content = var.ansible_vault_password
  file_permission = "0400"
}

resource "local_file" "inventory" {
  depends_on = [
    local_file.ansible_password,
    local_file.ssh_key
  ]
  filename = "${dirname(dirname(abspath(path.module)))}/ansible/hosts.yaml"
  sensitive_content = yamlencode({
    all = {
      hosts = {
        wireguard = {
          ansible_host = digitalocean_droplet.wireguard_server.ipv4_address
          ansible_user = "root"
        }
      }
    }
  })

  provisioner "local-exec" {
    working_dir = dirname(dirname(abspath(path.module)))
    command = "scripts/machine-startup.sh"
    environment = {
      ANSIBLE_STRATEGY_PLUGINS = var.mitogen.enable ? var.mitogen.path : ""
      ANSIBLE_STRATEGY = var.mitogen.enable ? "mitogen_linear" : "linear"
      ANSIBLE_GATHERING = "smart"
      ANSIBLE_FORKS = "20"
      ANSIBLE_SSH_PIPELINING = "true"
      ANSIBLE_SSH_TRANSFER_METHOD = "piped"
      ANSIBLE_SSH_ARGS = "-o ControlMaster=auto -o ControlPersist=15m -o StrictHostKeyChecking=no"
    }
  }
}
