terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "1.22.2"
    }

    local = {
      source = "hashicorp/local"
      version = "1.4.0"
    }
  }

  backend "local" {
    path = "../../state/machine/terraform.tfstate"
  }
}

provider "digitalocean" {
  token = var.token
}

provider "local" {

}
