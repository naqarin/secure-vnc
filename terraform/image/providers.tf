terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "1.22.2"
    }
  }

  backend "local" {
    path = "../../state/image/terraform.tfstate"
  }
}

provider "digitalocean" {
  token = var.token
}
