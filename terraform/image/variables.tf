variable "token" {
  description = "Digital ocean token"
  type = string
  validation {
    condition = can(regex("^[0-9a-f]{64}$", var.token))
    error_message = "Token must be a 64-characters hex lowercase string."
  }
}

variable "region" {
  description = "Region to create volume in."
  type = string
}

variable "volume_name" {
  description = "Users volume name."
  type = string
}

variable "mitogen" {}
variable "public_key" {}
variable "private_key" {}
variable "user_volume_id" {}
variable "image_name" {}
variable "ansible_vault_password" {}
