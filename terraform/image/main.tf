resource "digitalocean_volume" "users_home" {
  name = var.volume_name
  region = var.region
  size = 60
  initial_filesystem_type = "ext4"
}