variable "token" {
  type = string
}

variable "region" {
  type = string
}

variable "image_name" {
  type = string
}

variable "volume_name" {}
variable "mitogen" {}
variable "public_key" {}
variable "private_key" {}
variable "user_volume_id" {}
variable "ansible_vault_password" {}

source "digitalocean" "wireguard" {
  api_token = var.token
  image = "debian-9-x64"
  region = var.region
  size = "s-1vcpu-1gb"

  communicator = "ssh"
  ssh_username = "root"

  snapshot_name = var.image_name
  snapshot_regions = [
    var.region
  ]
}

build {
  sources = [
    "source.digitalocean.wireguard"
  ]

  provisioner "ansible" {
    galaxy_file = "../ansible/requirements.yaml"
    playbook_file = "../ansible/build.yaml"
  }
}