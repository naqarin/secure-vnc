#!/bin/bash

set -euo pipefail;
# shellcheck source=./utils.sh
source "$(dirname "$0")/utils.sh";
# shellcheck source=./image.sh
source "$(dirname "$0")/image.sh"

apply_users_volume() {
  decrypt_and_run "../../configuration" "../../variables.hcl" "terraform apply -var-file=../../variables.hcl -auto-approve";
}

main() {
  go_to_volume_directory;
  init_terraform_if_needed;
  apply_users_volume;
}

main
