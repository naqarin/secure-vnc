#!/bin/bash

set -euo pipefail;
# shellcheck source=./utils.sh
source "$(dirname "$0")/utils.sh";

go_to_ansible_directory() {
  local current_folder;
  current_folder="$(dirname "$0")";
  cd "${current_folder}/../ansible" || exit 1;
}

main() {
  go_to_ansible_directory;
  start_ssh_agent_and_erase_key "./terraform";
  run_and_erase_multiple_files "./hosts.yaml;./password" "ansible-playbook --vault-id vnc@password -i hosts.yaml startup.yaml";
  stop_ssh_agent;
}

main;
