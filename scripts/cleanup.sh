#!/bin/bash

set -euo pipefail;
# shellcheck source=./utils.sh
source "$(dirname "$0")/utils.sh";

main() {
  shred --force --iterations 5 --remove --zero "$(dirname "$0")/../result/vnc.conf"
}