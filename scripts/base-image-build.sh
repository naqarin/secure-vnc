#!/bin/bash

set -euo pipefail;
# shellcheck source=./utils.sh
source "$(dirname "$0")/utils.sh";

go_to_packer_directory() {
  local current_folder;
  current_folder="$(dirname "$0")";
  cd "${current_folder}/../packer" || exit 1;
}

parse_variables() {
  decrypt "../configuration" "../variables.hcl";
  access_token="$(hclq get token -r -i ../variables.hcl)";
  region="$(hclq get region -r -i ../variables.hcl)";
  image_name="$(hclq get image_name -r -i ../variables.hcl)";

  export access_token;
  export region;
  export image_name;
  erase "../variables.hcl";
}

get_images_count() {
  local images_count;
  images_count="$(doctl compute snapshot list --access-token="${access_token}" --region="${region}" --no-header --format=Name | grep "${image_name}" | wc -l)";

  printf "%d" "${images_count}";
}

print_already_exist() {
  log_info "Skipping image build" "Image already exists.";
}

build_packer_image() {
  decrypt_and_run "../configuration" "../variables.hcl" "packer build -var-file=../variables.hcl main.pkr.hcl";
}

main() {
  go_to_packer_directory;
  parse_variables;
  if [[ $(get_images_count) -gt 0 ]];
  then
    print_already_exist;
  else
    build_packer_image;
  fi
}

main
