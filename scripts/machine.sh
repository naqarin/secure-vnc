#!/bin/bash

go_to_machine_directory() {
  local current_folder;
  current_folder="$(dirname "$0")";
  cd "${current_folder}/../terraform/machine" || exit 1;
}
