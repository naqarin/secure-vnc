#!/bin/bash

FORMATTING_COLOR_BOLD_BLUE="\033[1;34m";
FORMATTING_COLOR_BLUE="\033[0;34m";
FORMATTING_RESET_ALL="\033[1;0m";

log_info() {
  local text;
  local header;
  header="$1";
  shift;
  text="$*";

  echo -e "${FORMATTING_COLOR_BOLD_BLUE}${header}:${FORMATTING_RESET_ALL}${FORMATTING_COLOR_BLUE} ${text}${FORMATTING_RESET_ALL}";
}

erase_with_delay() {
  local delay;
  local target;
  delay="$1";
  target="$2";

  sleep "${delay}";
  shred --force --iterations 5 --remove --zero "${target}";
}

erase_multiple_with_delay() {
  local delay;
  local targets_string;
  delay="$1";
  targets_string="$2";
  readarray -td';' targets < <(printf "%s" "${targets_string}");
  declare -p targets;

  sleep "${delay}";
  for target in "${targets[@]}";
  do
    shred --force --iterations 5 --remove --zero "${target}";
  done
}

decrypt_and_run() {
  local in;
  local out;
  local command;
  in="$1";
  shift;
  out="$1";
  shift;
  command="$*";

  if [[ -z ${GPG_TTY+x} ]];
  then
    GPG_TTY=$(tty);
    export GPG_TTY;
  fi
  base64 -d "${in}" | gpg --cipher-algo AES256 --output "${out}" --decrypt -;
  (erase_with_delay 5 "${out}")&
  bash -c "${command}";
}

run_and_erase_multiple_files() {
  local secret_files;
  local command;
  secret_files="$1";
  command="$2";

  (erase_multiple_with_delay 5 "${secret_files}")&
  bash -c "${command}";
}

start_ssh_agent_and_erase_key() {
  local key_file;
  key_file="$1";

  eval "$(ssh-agent -s)";
  (erase_with_delay 5 "${key_file}")&
  ssh-add "${key_file}";
  export SSH_AGENT_PID;
}

stop_ssh_agent() {
  kill "${SSH_AGENT_PID}";
}

decrypt() {
  local in;
  local out;
  in="$1";
  out="$2";

  if [[ -z ${GPG_TTY+x} ]];
  then
    GPG_TTY=$(tty);
    export GPG_TTY;
  fi
  base64 -d "${in}" | gpg --cipher-algo AES256 --output "${out}" --decrypt -;
}

erase() {
  local target;
  target="$1";

  shred --force --iterations 5 --remove --zero "${target}";
}

init_terraform_if_needed() {
  if [[ ! -d .terraform ]];
  then
    terraform init;
  fi
}