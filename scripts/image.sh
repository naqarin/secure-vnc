#!/bin/bash

go_to_volume_directory() {
  local current_folder;
  current_folder="$(dirname "$0")";
  cd "${current_folder}/../terraform/image" || exit 1;
}
