#!/bin/bash

set -euo pipefail;
# shellcheck source=./utils.sh
source "$(dirname "$0")/utils.sh";
# shellcheck source=./machine.sh
source "$(dirname "$0")/machine.sh"

destroy_machine() {
  decrypt_and_run "../../configuration" "../../variables.hcl" "terraform destroy -var-file=../../variables.hcl -auto-approve";
}

main() {
  go_to_machine_directory;
  destroy_machine;
}

main
