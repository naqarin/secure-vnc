#!/bin/bash

set -euo pipefail;
# shellcheck source=./utils.sh
source "$(dirname "$0")/utils.sh";
# shellcheck source=./image.sh
source "$(dirname "$0")/image.sh"

destroy_users_volume() {
  decrypt_and_run "../../configuration" "../../variables.hcl" "terraform destroy -var-file=../../variables.hcl -auto-approve";
}

main() {
  go_to_volume_directory;
  destroy_users_volume;
}

main
